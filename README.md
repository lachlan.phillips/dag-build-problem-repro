# DAG build problem repro

This is a build for two subsystems: an "infrastructure" system, and an "app"
built on top of it.

There are four build stages, though for build efficiency we use a DAG to express
the dependencies between these jobs:

1. An rpm is built for each, which can be done separately.
2. The infrastructure system builds a base container.
3. The two systems build their final container on top of the shared one.
4. Tests are run against the infrastructure alone, and a combined stack.

Our problem is this: when the `app:rpm` job fails, all jobs that depend on it
are skipped EXCEPT the last one: `app:test`. This job has dependencies on both
`app:rpm` and the skipped stage 3 container builds, yet it still attempts to
execute.

## Resolution

GitLab has apparently independently fixed this bug a few days after I posted this.